using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MissionCallout : MonoBehaviour
{
	private RectTransform rect;
	public enum CallType {start,complete ,whirl,laser,end}
	private CallType callType;

	[SerializeField] private TextMeshProUGUI annoucerText;
	private CanvasManager canvasManager;
	private void Awake()
	{
		canvasManager = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>();
		rect = GetComponent<RectTransform>();
	}
	private void Start()
	{
		rect.anchoredPosition = new Vector2(-616, 0);
	}

	public void rollIn(string _annoucerText, CallType _callType)
	{
		annoucerText.text = _annoucerText;

		rect.LeanMove(new Vector3(0, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setFrom(new Vector3(-616, 0, 0)).setOnComplete(rollOut,_callType);
	}

	public void rollOut(object _callType)
	{
		AudioManager.instance.LerpMute("BossTheme");
		if(_callType.ToString() == "start")
		{
			rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f);
		}
		else if(_callType.ToString() == "complete")
		{
			rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(2.5f).setOnComplete(transOut);
		}
		else if (_callType.ToString() == "whirl")
		{
			GameManager.instance.stageReached = 2;
			rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f).setOnComplete(transOut1);
		}
		else if (_callType.ToString() == "laser")
		{
			GameManager.instance.stageReached = 3;
			rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f).setOnComplete(transOut2);
		}
		else if (_callType.ToString() == "end")
		{
			GameManager.instance.stageReached = 4;
			rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f).setOnComplete(transOut3);
		}
	}

	public void transOut()
	{
		canvasManager.TransOut("Base");
		SaveManager.save(GameManager.instance);
	}

	public void transOut1()
	{
		if (GameManager.instance.allowWhirl)
		{
			canvasManager.TransOut("Base");
			SaveManager.save(GameManager.instance);
		}
		else
		{
			canvasManager.TransOut("UnlockWhirlWind");
			GameManager.instance.allowWhirl = true;
			SaveManager.save(GameManager.instance);
		}

	}

	public void transOut2()
	{
		if (GameManager.instance.allowLaser)
		{
			canvasManager.TransOut("Base");
			SaveManager.save(GameManager.instance);
		}
		else
		{
			canvasManager.TransOut("UnlockLaser");
			GameManager.instance.allowLaser = true;
			SaveManager.save(GameManager.instance);
		}

	}
	public void transOut3()
	{
		canvasManager.TransOut("End");
		SaveManager.save(GameManager.instance);
	}


}
