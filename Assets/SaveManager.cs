using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveManager
{
	public static void save(GameManager game)
	{
		BinaryFormatter formatter = new BinaryFormatter();
		string path = Application.persistentDataPath + "/saveData.SAVE";
		FileStream stream = new FileStream(path, FileMode.Create);

		SaveData save = new SaveData(game);
		formatter.Serialize(stream, save);
		stream.Close();
	}

	public static SaveData load()
	{
		string path = Application.persistentDataPath + "/saveData.SAVE";
		if (File.Exists(path))
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream stream = new FileStream(path, FileMode.Open);


			SaveData save = formatter.Deserialize(stream) as SaveData;
			stream.Close();

			return save;
		}
		else
		{
			return null;
		}

	}
}
