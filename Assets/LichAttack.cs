using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LichAttack : EnemyAttack
{

    public enum AttackState {circling, center , sideway ,transition}

    [Header("BossStat")]
    [SerializeField] private float moveTimer = 1;
    [SerializeField] private AttackState attackState;
    [SerializeField] private float bulletForce;
    [SerializeField] private float shotCooldown = 0;
    [SerializeField] private float BurstshotCooldown = 0;
    private float _shotCooldown;
    private float _BurstshotCooldown;

    [Header("Reference Object")]
    [SerializeField] private GameObject bulletPoint;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject superBullet;
    [SerializeField] private GameObject centerWaypoint;
    [SerializeField] private List<GameObject> waypoint = new List<GameObject>();
    [SerializeField] private List<GameObject> Sidewaypoint = new List<GameObject>();

    private bool charging = false;
    private int currentWaypoint;
    private float _moveTimer;
    private bool getTarget = false;
    private bool initiate = false;
    private bool enableRotation = false;

    // Start is called before the first frame update
    void Start()
    {
        _moveTimer = moveTimer;
        _shotCooldown = shotCooldown;
        _BurstshotCooldown = BurstshotCooldown;
   
        Initialize();
    }

	// Update is called once per frame
	void Update()
    {
        if (!active)
        {
            return;
        }
        moveTimer -= Time.deltaTime;

        if (moveTimer <= 0)
        {
            initiate = false;
            getTarget = false;
            enableRotation = false;
            charging = false;
            attackState = AttackState.transition;
            StartCoroutine(randomAttackState()) ;
            moveTimer = _moveTimer;
        }

        switch (attackState)
        {
            case AttackState.transition:
                break;
            case AttackState.center:

                if (initiate == false)
                {
                    AIpath.maxSpeed = 8;
                    AIDestinationSetter.target = centerWaypoint.transform;
					if (AIpath.reachedDestination)
					{
                        initiate = true;
                    }
                }
                if (initiate == true)
                {
                    enableRotation = true;

                }

                break;

            case AttackState.circling:
				if (initiate == false)
                {
                    AIpath.maxSpeed = 8;
                    int random = Random.Range(0, 8);
                    AIDestinationSetter.target = waypoint[random].transform;
                    currentWaypoint = random;
					if (AIpath.reachedDestination)
					{
                        initiate = true;
                    }
                }

                if (initiate == true)
                {
                    BurstshotCooldown -= Time.deltaTime;

                    if (BurstshotCooldown <= 0)
                    {
                       StartCoroutine(BurstShot(5));
                        BurstshotCooldown = _BurstshotCooldown;
                    }

                    if (AIpath.reachedDestination)
                    {
                        currentWaypoint = (currentWaypoint + 1) % waypoint.Count;
                        AIDestinationSetter.target = waypoint[currentWaypoint].transform;
                    }
                }

                break;

            case AttackState.sideway:
                if (initiate == false)
                {
                    AIpath.maxSpeed = 8;

                    if(getTarget == false)
                    {
                        int random = Random.Range(0, 2);
                        AIDestinationSetter.target = Sidewaypoint[random].transform;
                        getTarget = true;
                    }
   
                    if (AIpath.reachedDestination && getTarget == true)
                    {
                        initiate = true;
                        charging = false;
                    }

                }

                if (initiate == true)
                {
                    if(charging == false)
					{
                        FireSuperBullet();
                    }
                }
                break;
        }
    }

	private void FixedUpdate()
	{
		if (enableRotation)
		{
            bulletPoint.transform.Rotate(0, 0, 5);

            shotCooldown -= Time.deltaTime;

            if (shotCooldown <= 0)
            {
                ShootBullet();
                shotCooldown = _shotCooldown;
            }
        }
	}

    private void FireSuperBullet()
	{
        charging = true;
        GameObject _superBullet = Instantiate(superBullet, transform.position, Quaternion.identity);
        LeanTween.scale(_superBullet, new Vector3(20, 20, 20), 2f).setEase(LeanTweenType.easeInBounce).setOnComplete(launch, _superBullet);
    }

	private void ShootBullet()
    {
        Vector2 bulDir = bulletPoint.transform.up;

        GameObject bulletInstance = Instantiate(bullet, bulletPoint.transform.position, bulletPoint.transform.rotation);
        Rigidbody2D rb = bulletInstance.GetComponent<Rigidbody2D>();
        rb.AddForce(bulDir * bulletForce /  2, ForceMode2D.Impulse);
    }

    IEnumerator BurstShot(int bulletAmount)
	{
        for (int i = 0; i < bulletAmount; i++)
		{
            Vector2 n = ((Vector2)player.transform.position - (Vector2)bulletPoint.transform.position).normalized;
            GameObject b = Instantiate(bullet, transform.position, Quaternion.identity);
            b.GetComponent<Rigidbody2D>().AddForce(n * bulletForce, ForceMode2D.Impulse);
            yield return new WaitForSeconds(0.2f);
		}
	}
    private void launch(object _object)
	{
        Vector2 n = ((Vector2)player.transform.position - (Vector2)bulletPoint.transform.position).normalized;
        GameObject _SuperD = _object as GameObject;
        _SuperD.GetComponent<CircleCollider2D>().enabled = true;
        _SuperD.GetComponent<Rigidbody2D>().AddForce(n * bulletForce, ForceMode2D.Impulse);
        charging = false;
	}

    IEnumerator randomAttackState()
	{
        AttackState _attackState = (AttackState)Random.Range(0, 3);

        while(_attackState == attackState)
		{
            _attackState = (AttackState)Random.Range(0, 3);
        }

        yield return new WaitForSeconds(0.3f);
        attackState = _attackState;
	}

}
