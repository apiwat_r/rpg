using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhirlWind : MonoBehaviour
{
	[SerializeField] PlayerSpecial playerSpecial;
	private void OnTriggerEnter2D(Collider2D collision)
	{
	
		if(collision.tag == "Enemy")
		{
			if (playerSpecial)
			{
				collision.GetComponent<EnemyStat>().takeDamage(playerSpecial.whirlDamage);
			}
			else
			{
				collision.GetComponent<EnemyStat>().takeDamage(5);
			}
		
		}

		if(collision.tag == "Bullet")
		{
		
			if(collision.GetComponent<Bullet>().returnType() == Bullet.BulletType.enemy)
			{
				Destroy(collision.gameObject);
			}			
		}
	}
}
