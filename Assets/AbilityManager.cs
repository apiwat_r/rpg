using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AbilityManager : MonoBehaviour
{
	[SerializeField] RectTransform rect;
	[SerializeField] TextMeshProUGUI text;
	[SerializeField] PlayerSpecial player;
	[SerializeField] List<GameObject> obj = new List<GameObject>();
	[SerializeField] List<GameObject> pos = new List<GameObject>();
	[SerializeField] private GameObject transiosionScreen;

	private CanvasManager canvasManager;
	private Animator animator;
	private bool rolling = false;
	public enum AnimationShow {whirlwind, laser };
	public AnimationShow animationShow;

	private void Start()
	{
		AudioManager.instance.Play("AbilityShowcase",0.1f);
		canvasManager = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>();
		animator = GetComponent<Animator>();
		if(animationShow == AnimationShow.whirlwind)
		{
			GameManager.instance.allowWhirl = true;
			animator.Play("Controller2");
		}

		if(animationShow == AnimationShow.laser)
		{
			GameManager.instance.allowLaser = true;
			animator.Play("Controller");
		}
	}

	public void rollIn()
	{
		rect.LeanMove(new Vector3(0, 64, 0), 1f).setEase(LeanTweenType.easeOutCubic).setFrom(new Vector3(-616, 64, 0)).setOnComplete(rollOut);
	}

	public void rollOut(object _callType)
	{
			rect.LeanMove(new Vector3(1259, 64, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f);
	}

	public void changeText(string _string)
	{
		text.text = _string;
	}

	public void doWhirl()
	{
		player.activateWhirl();
	}

	public  void doLaser()
	{
		player.activateLaser();
	}

	public void whirlDemon()
	{
		for(int i = 0; i < obj.Count; i++)
		{
			obj[i].LeanMove(pos[i].transform.position, 1f);
		}
	}

	public void TransOut(string _sceneName)
	{
		LeanTween.alpha(transiosionScreen, 1, 0.5f).setOnComplete(TransIn, _sceneName);
		AudioManager.instance.LerpMute("AbilityShowcase");
	}
	public void TransIn(object sceneName = null)
	{
		if (sceneName != null)
		{
			SceneManager.LoadScene(sceneName.ToString());
		}
		//transiosionScreen.GetComponent<Image>().color = new Color(0, 0, 0, 1);
		LeanTween.alpha(transiosionScreen, 0, 0.5f);
	}
}
