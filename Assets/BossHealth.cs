using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BossHealth : MonoBehaviour
{
	private RectTransform rect;
	[SerializeField] private Image bossHealthBar;
	[SerializeField] private MissionCallout missionCallout;
	[SerializeField] private TextMeshProUGUI bossName;
	private float maxhealth = 0;
	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		EnemyAttack.callBossHealth += BossHealthRollIn;
		EnemyStat.updateBossHealth += BossHealthUpdate;
	}

	private void OnDestroy()
	{
		EnemyAttack.callBossHealth -= BossHealthRollIn;
		EnemyStat.updateBossHealth -= BossHealthUpdate;
	}

	public void rollOut()
	{
		rect.LeanMove(new Vector3(616, 0, 0), 1f).setEase(LeanTweenType.easeOutCubic).setDelay(1.5f);
	}

	public void BossHealthRollIn(string _bossName)
	{
		bossName.text = _bossName;
		rect.LeanMove(new Vector3(-9, -174, 0), 1f).setEase(LeanTweenType.easeOutCubic).setFrom(new Vector3(-9,-236,0));
	}

	public  void BossHealthUpdate(float _remainingHealth, MissionCallout.CallType _callType)
	{
		bossHealthBar.fillAmount = _remainingHealth;

		if(_remainingHealth == 0)
		{
			missionCallout.rollIn("Mission Completed!", _callType);
		}
	}
}
