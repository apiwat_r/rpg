using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using System;

public class SkullWall : MonoBehaviour
{
	public static Action<string> callAnnoucer;
	private Vector3 originPos;
	[SerializeField] EnemyAttack enemyAttack;
	[Header("SkullMove")]
	[SerializeField] private float SkullmoveAmountX = 0f;
	[SerializeField] private float SkullmoveAmountY = 0f;
	[SerializeField] private float SkullCloseX = 0f;
	[SerializeField] private float SkullCloseY = 0f;
	[Header("HeroMove")]
	[SerializeField] private  float moveAmountX = 0f;
	[SerializeField] private float moveAmountY = 0f;
	[SerializeField] private DialougeGraph graph;
	private MissionCallout missionCallout;
	private NodeParser nodeParser;
	private CinemachineBrain cinemachineBrain;
	private bool active = false;

	
	private void Awake()
	{

		missionCallout = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>().returnMission();
		cinemachineBrain = GameObject.FindWithTag("MainCamera").GetComponent<CinemachineBrain>();
		nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
	}
	void Start()
    {
		missionCallout.rollIn("Mission start",MissionCallout.CallType.start);
		originPos = transform.position;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.transform.tag == "Player"  && !active)
		{
		

			transform.LeanMove(transform.position + new Vector3(SkullmoveAmountX, SkullmoveAmountY, 0f), 0.5f).setEase(LeanTweenType.easeInExpo).setOnComplete(bossEnter, collision.gameObject);

			active = true;

			nodeParser.ConvoOn();
		}
	}

	public void bossEnter(object _player)
	{
		GameObject player = _player as GameObject;

		player.LeanMove(player.transform.position + new Vector3(moveAmountX, moveAmountY, 0), 1.5f).setOnComplete(wallClose);
	}

	public void wallClose()
	{
		StartCoroutine(camYield());

	}

	IEnumerator camYield()
	{
		AudioManager.instance.LerpMute("DungeonTheme");
		yield return new WaitUntil(() => cinemachineBrain.IsBlending == false);
		transform.position = originPos;
		transform.LeanMove(transform.position + new Vector3(SkullCloseX, SkullCloseY, 0f), 0.5f);
		yield return new WaitForSeconds(1f);
		nodeParser.GetGraph(graph);
		nodeParser.GetEnemy(enemyAttack);
		nodeParser.AutoConver();
	}

}
