using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
	private Animator animator;

	[SerializeField] private CapsuleCollider2D capsuleCollider2D;
	[SerializeField] private float timer;

	private void Start()
	{
		animator = GetComponent<Animator>();
		StartCoroutine(countDown(timer));
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag == "Player")
		{
			collision.GetComponent<PlayerStat>().DamageHealth(1);
		}
	}

	IEnumerator countDown(float _timer)
	{
		yield return new WaitForSeconds(_timer);
		animator.Play("Spawn");
	}

	public void colliderActivate()
	{
		capsuleCollider2D.enabled = !capsuleCollider2D.enabled;
	}

	private void DeleteBomb()
	{
		Destroy(gameObject);
	}


}
