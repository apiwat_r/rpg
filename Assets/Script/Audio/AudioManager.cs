using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

	public static AudioManager instance;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

		foreach(Sound s in sounds)
		{
			s.source = gameObject.AddComponent<AudioSource>();
			s.source.clip = s.clip;
			s.source.volume = s.volume;
			s.source.pitch = s.pitch;
			s.source.loop = s.loop;
			s.source.outputAudioMixerGroup = s.mixer;
		}
	}

	private void Start()
	{

	}
	// Update is called once per frame
	public void Play(string name, float volume = 0.5f)
	{
		Sound s = Array.Find(sounds, sound => sound.name == name);
		if(s == null)
		{
			return;
		}
		s.source.Play();
		s.source.volume = volume;
	}

	public void LerpMute(string name)
	{
		Sound s = Array.Find(sounds, sound => sound.name == name);
		if (s == null)
		{
			return;
		}
		LeanTween.value(s.source.volume, 0, 0.5f).setEase(LeanTweenType.linear).setOnUpdate((float val) => { s.source.volume = val; });
	}
	
}
