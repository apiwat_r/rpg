using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    private SoundAdjust soundAdjust;
	private Image buttonImage;
	[SerializeField]private bool music = true;
	[SerializeField]private bool sfx = true;
	

  
    void Start()
    {
        soundAdjust = GameObject.FindGameObjectWithTag("AudioManager").GetComponent<SoundAdjust>();
		buttonImage = transform.GetComponent<Image>();
    }
    void Update()
    {
		if (music)
		{
			if (!soundAdjust.musicToggle)
			{
				buttonImage.color = new Color32(85, 85, 85, 255);
			}
			else
			{
				buttonImage.color = Color.white;
			}
		}

		if (sfx)
		{
			if (!soundAdjust.sfxToggle)
			{
				buttonImage.color = new Color32(85, 85, 85, 255);
			}
			else
			{
				buttonImage.color = Color.white;
			}
		}

	}

    public void musicSet()
	{
        soundAdjust.setMusic();
	}

    public void sfxSet()
	{
        soundAdjust.setSFX();
    }

    // Update is called once per frame

}
