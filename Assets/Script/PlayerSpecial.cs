using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerSpecial : MonoBehaviour
{
	GameManager gameManager;
	public static Action<float> CallWhirlCooldown;
	public static Action<float> CallLaserCooldown;
	[SerializeField]Animator playeranimator;
	[SerializeField] Animator whirlWindAnimator;
	[SerializeField] CircleCollider2D whirlwindCollider;

	private bool whirlOnCooldown = false;
	private bool laserOnCooldown = false;

	private float _whirlCooldown = 0f;
	private float _laserCoolDown = 0f;

	private float whirlCoolDown = 1f;
	private float laserCoolDown = 1f;

	private int laserDamage = 0;
	public int whirlDamage { get; private set; } = 0;

	public bool isWhirling { get; private set; } = false;
	public bool isLasering { get; private set; } = false;

	private Camera cam;
	private Vector3 mousePos;
	[SerializeField] private ParticleSystem particle;
	[SerializeField]private LineRenderer lineRenderer;
	[SerializeField] private bool fake = false;

	private void Start()
	{
		gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
		cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
		checkUpgradeLevel();
		_whirlCooldown = whirlCoolDown;
		_laserCoolDown = laserCoolDown;
	}
	private void Update()
	{
		if(gameManager == null)
		{
			return;
		}

		getPoint();
		lineRenderer.SetPosition(0, transform.position);
		if (Input.GetKeyDown(KeyCode.Q) && gameManager.allowWhirl && !whirlOnCooldown)
		{
			CallWhirlCooldown(_whirlCooldown);
			whirlOnCooldown = true;
			StartCoroutine(whirlwind());
			playeranimator.Play("WhirlWind");
			whirlWindAnimator.Play("WhirlWindDual");
			whirlwindCollider.enabled = true;
			AudioManager.instance.Play("Whirl");
		}

		if (Input.GetKeyDown(KeyCode.E) && gameManager.allowLaser && !laserOnCooldown)
		{
			laserOnCooldown = true;
			CallLaserCooldown(_laserCoolDown);
			StartCoroutine(Charge(fake));
		}

		cooldownCal();
	}

	IEnumerator Charge(bool _fake)
	{

		var emission = particle.emission;
		emission.enabled = true;
		AudioManager.instance.Play("Charge");
		yield return new WaitForSeconds(0.7f);
		Laser(_fake);
	}

	IEnumerator LaserRender(bool _fake)
	{
		if (fake)
		{
			lineRenderer.SetPosition(1, ((transform.position + new Vector3(80, 0, 0)) - transform.position).normalized * 1000);
		}
		else
		{
			lineRenderer.SetPosition(1, (mousePos - transform.position).normalized * 1000);
		}
		LeanTween.value(lineRenderer.startWidth, 0.3f, 0.1f).setEase(LeanTweenType.linear).setOnUpdate((float val) => { lineRenderer.startWidth = val; });
		LeanTween.value(lineRenderer.endWidth, 0.3f, 0.1f).setEase(LeanTweenType.linear).setOnUpdate((float val) => { lineRenderer.startWidth = val; });
		yield return new WaitForSeconds(0.2f);
		LeanTween.value(lineRenderer.startWidth, 0f, 0.1f).setEase(LeanTweenType.easeOutCirc).setOnUpdate((float val) => { lineRenderer.startWidth = val; });
		LeanTween.value(lineRenderer.endWidth, 0f, 0.1f).setEase(LeanTweenType.easeOutCirc).setOnUpdate((float val) => { lineRenderer.startWidth = val; });
	}

	public  void Laser(bool _fake)
	{
		AudioManager.instance.Play("Laser");
		StartCoroutine(LaserRender(_fake));
		RaycastHit2D[] hits;

		if(fake == true)
		{
			hits = Physics2D.RaycastAll(transform.position, ((transform.position + new Vector3(80,0,0)) - transform.position).normalized, 1000);
		}
		else
		{
			hits = Physics2D.RaycastAll(transform.position, (mousePos - transform.position).normalized, 1000);
		}
		
		
		for (int i =0; i < hits.Length; i++)
		{
			RaycastHit2D hit = hits[i];

			print(hit);

			EnemyStat d = hit.transform.GetComponent<EnemyStat>();
			if (d)
			{
				d.takeDamage(laserDamage);
			}
		}

		var emission = particle.emission;
		emission.enabled = false;
	}

	IEnumerator whirlwind()
	{
		whirlwindCollider.enabled = true;
		isWhirling = true;
		yield return new WaitForSeconds(0.3f);
		whirlwindCollider.enabled = false;
		isWhirling = false;
	}

	private void cooldownCal()
	{
		if (whirlOnCooldown)
		{
			_whirlCooldown -= Time.deltaTime;
			CallWhirlCooldown(_whirlCooldown/ whirlCoolDown);
			if (_whirlCooldown <= 0)
			{
				whirlOnCooldown = false;
				_whirlCooldown = whirlCoolDown;
			}
		}

		if (laserOnCooldown)
		{
			_laserCoolDown -= Time.deltaTime;
			CallLaserCooldown(_laserCoolDown/laserCoolDown);
			if (_laserCoolDown <= 0)
			{
				laserOnCooldown = false;
				_laserCoolDown = laserCoolDown;
			}
		}
	}

	public void activateWhirl()
	{
		playeranimator.Play("WhirlWind");
		whirlWindAnimator.Play("WhirlWindDual");
		AudioManager.instance.Play("Whirl");
		StartCoroutine(whirlwind());
	}

	public void activateLaser()
	{
		StartCoroutine(Charge(fake));
	}

	public void getPoint()
	{
		mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
		mousePos.z = -2;
	}

	public void checkUpgradeLevel()
	{
		switch (gameManager.circleLevel)
		{
			case 1:
				whirlCoolDown = 7f;
				whirlDamage = 3;
				break;
			case 2:
				whirlCoolDown = 6f;
				whirlDamage = 4;
				break;
			case 3:
				whirlCoolDown = 5f;
				whirlDamage = 4;
				break;
			case 4:
				whirlCoolDown = 4.5f;
				whirlDamage = 5;
				break;
		}
		switch (gameManager.railLevel)
		{
			case 1:
				laserCoolDown = 7f;
				laserDamage = 4;
				break;
			case 2:
				laserCoolDown = 6f;
				laserDamage = 5;
				break;
			case 3:
				laserCoolDown = 5f;
				laserDamage = 6;
				break;
			case 4:
				laserCoolDown = 4.5f;
				laserDamage = 7;
				break;
		}
	}
}
