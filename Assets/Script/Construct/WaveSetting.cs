using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WaveSetting 
{
    public List<EnemySetting> enemyType = new List<EnemySetting>();
}

[System.Serializable]
public class EnemySetting 
{
    public GameObject enemy;
    public int amount;
}

