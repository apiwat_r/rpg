using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Money : Item
{
    [SerializeField] private int moneyAmount;
    [SerializeField]Vector2 direction;

    void Start()
    {
        rangeDetector = GetComponent<RangeDetector>();
        player = GameObject.FindWithTag("Player");
        rb = GetComponent<Rigidbody2D>();

        direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        ItemFly();

    }

    private void Update()
    {
        rb.MovePosition(rb.position + direction * forceAmount * Time.deltaTime);
    }

	private void FixedUpdate()
	{
        CheckRange();
	}

	public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            GameManager.instance.addMoney(moneyAmount);
            AudioManager.instance.Play("Coin");
            Destroy(gameObject);
        }
    }

    public override void ItemFly()
    {
        LeanTween.value(gameObject, forceAmount, 0, 1f).setEase(LeanTweenType.easeInOutBack).setOnUpdate((float val) => { forceAmount = val; }); 
        
    }
}
