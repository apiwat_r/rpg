using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    protected RangeDetector rangeDetector;
    protected GameObject player;
    protected Rigidbody2D rb;
    [SerializeField]protected float forceAmount;
    [SerializeField] protected bool moving = true;

	public virtual void OnTriggerEnter2D(Collider2D collision)
	{
        if (collision.tag == "Player")
        {
            Destroy(gameObject);
        }
    }

    public virtual void ItemFly()
	{
        LeanTween.value(gameObject,forceAmount, 0, 0f).setOnUpdate((float val) => { forceAmount = val; }); ;
    }

    public virtual void CheckRange()
	{
        if (rangeDetector.inRange)
        {
            this.transform.position = Vector3.MoveTowards(this.transform.position, player.transform.position, 0.1f);
        }
    }
}
