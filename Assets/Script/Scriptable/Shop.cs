using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Upgrade", menuName = "Upgrade")]
public class Shop : ScriptableObject
{
    public string upgradeName;
    public string upgradeDescription;
    public Sprite upgradeSprite;
    public int upgradeID;

    public List<int> cost = new List<int>();
}
