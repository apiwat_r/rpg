using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using TMPro;

public class MusicToggle : MonoBehaviour
{
	[SerializeField] private AudioMixer Mixer;
	[SerializeField] private TextMeshProUGUI sfxText;
	[SerializeField] private TextMeshProUGUI MusicText;
	public bool musicToggle { get; private set; } = true;
	public bool sfxToggle { get; private set; } = true;

	public void setMusic()
	{
		musicToggle = !musicToggle;

		if (musicToggle == false)
		{
			Mixer.SetFloat("MusicVolume", -80);
			MusicText.text = "Music: OFF";
		}
		else
		{
			Mixer.SetFloat("MusicVolume", 0);
			MusicText.text = "Music: On";
		}

	}
	public void setSFX()
	{
		sfxToggle = !sfxToggle;

		if (sfxToggle == false)
		{
			Mixer.SetFloat("SFXVolume", -80);
			sfxText.text = "SFX: OFF";
		}
		else
		{
			Mixer.SetFloat("SFXVolume", 0);
			sfxText.text = "SFX: On";

		}

	}
}
