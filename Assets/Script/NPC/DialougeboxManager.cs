using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialougeboxManager : MonoBehaviour
{
	private void Start()
	{
        SetTargetInvisible();
	}

	// Start is called before the first frame update
	public void SetTargetInvisible()
    {
        Component[] a = gameObject.GetComponentsInChildren(typeof(Image));
        foreach (Component b in a)
        {
            Image c = (Image)b;
            c.enabled = false;
        }

        Component[] t = gameObject.GetComponentsInChildren(typeof(TextMeshProUGUI));
        foreach (Component b in t)
        {
            TextMeshProUGUI c = (TextMeshProUGUI)b;
            c.enabled = false;
        }
    }

    public void SetTargetVisible()
	{
        Component[] a = gameObject.GetComponentsInChildren(typeof(Image));
        foreach (Component b in a)
        {
            Image c = (Image)b;
            c.enabled = true;
        }

        Component[] t = gameObject.GetComponentsInChildren(typeof(TextMeshProUGUI));
        foreach (Component b in t)
        {
            TextMeshProUGUI c = (TextMeshProUGUI)b;
            c.enabled = true;
        }
    }
}
