using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class DialougeUninteract : MonoBehaviour
{
    [SerializeField] private float textSpeed = 1f;
    [SerializeField] private TextMeshProUGUI dialouge;
    [SerializeField] private Animator animator;
    [SerializeField] private string music;
    IEnumerator _textCoroutine;

    private int dialougeIndex = -1;

    [TextArea(3,10)]
    [SerializeField] private string[] stringSet;
    // Start is called before the first frame update
    void Start()
    {
        AudioManager.instance.Play(music,0.1f);

        textSpeed = 1 / textSpeed;

        StartCoroutine("TypeText");
        
    }

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
            SceneManager.LoadScene("Base");
		}

        print(dialougeIndex + " " + stringSet.Length);
	}

	IEnumerator TypeText()
	{
        dialougeIndex += 1;

        if(dialougeIndex == stringSet.Length)
		{
            animator.Play("FadeOut");
            AudioManager.instance.LerpMute("End");
            yield break;
		}

        dialouge.text = "";

        char[] d = stringSet[dialougeIndex].ToCharArray();
        string b = stringSet[dialougeIndex];

        foreach (char letter in d)
		{
			dialouge.text += letter;

			if (dialouge.text == b)
			{
                yield return new WaitForSeconds(1.5f);
                StartCoroutine("TypeText");
			}

			yield return new WaitForSeconds(textSpeed);
		}
	}

    public void transition(string sceneName)
	{
        SceneManager.LoadScene(sceneName);
	}
}
