using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestCheck : MonoBehaviour
{
    [SerializeField] private bool quest = false;
    [SerializeField] private SpriteRenderer spriteRenderer;
    
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
		if (!quest)
		{
            spriteRenderer.enabled = false;
		}
		else
		{
            spriteRenderer.enabled = true;
		}
    }

    public void disableQuest()
	{
        quest = false;
	}

    public void enableQuest()
	{
        quest = true;
	}
}
