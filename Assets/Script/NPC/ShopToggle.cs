using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class ShopToggle : MonoBehaviour
{
	public static Action toggleShop;
	// Start is called before the first frame update
	private NodeParser nodeParser;
	private SpriteRenderer sprite;
	private RangeDetector rangeDetector;
	[SerializeField] private DialougeGraph graph;

	void Start()
    {
		sprite = GetComponent<SpriteRenderer>();
		rangeDetector = GetComponent<RangeDetector>();
        nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
    }

	private void Update()
	{
		if (rangeDetector.inRange)
		{
			sprite.material.SetFloat("_Thickness", 0.045f);
		}
		else
		{
			sprite.material.SetFloat("_Thickness", 0f);
		}

		if (rangeDetector.inRange && !nodeParser.inConvo && Input.GetKeyDown(KeyCode.E))
		{
			if (GameManager.instance.tutorial == false)
			{
				nodeParser.GetGraph(graph);
				StartCoroutine(nodeParser.InitiateConver());
				return;
			}

			toggleShop();
		}
	}

}
