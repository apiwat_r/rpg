using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraMove : MonoBehaviour
{
    private CinemachineVirtualCamera Vcam;

    private GameObject cam;

    [SerializeField] private bool checkPoint;

    // Start is called before the first frame update
    void Start()
    {
        Vcam = GetComponent<CinemachineVirtualCamera>();
        cam = GameObject.FindWithTag("CameraManager");
    }

	private void OnTriggerEnter2D(Collider2D collision)
	{

        if(checkPoint == true)
		{
            GameManager.instance.changeCheckPoint(GameManager.CheckPointState.CheckPoint);
		}
        if(cam == null)
		{
            return;
		}

        for(var i = 0; i <  cam.transform.childCount; i++)
	    {
            cam.transform.GetChild(i).GetComponent<CinemachineVirtualCamera>().Priority = 0;
	    }
        Vcam.Priority = 1;
	}
}
