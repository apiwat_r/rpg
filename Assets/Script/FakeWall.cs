using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class FakeWall : MonoBehaviour
{
	private Tilemap tilemap;
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (collision.transform.tag  == "Bullet")
		{
			Destroy(gameObject);
		}
	}
}
