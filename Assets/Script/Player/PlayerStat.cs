using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using TMPro;

public class PlayerStat : MonoBehaviour
{
	public static Action callDead;
	public static Action<float,bool> updateHealth;
    [SerializeField] private float health = 4;
    private float maxHealth = 4;

	private bool invul = false;
	public bool isDead { get; private set; } = false;

	[SerializeField] SpriteRenderer playerSprite;
	[SerializeField] Animator animator;

	private void Start()
	{
		updatehealth();
		health = maxHealth;
	}

	private void Update()
	{

		if (Input.GetKeyDown(KeyCode.Z))
		{
			RestoreHealth(2);
		}
		if (Input.GetKeyDown(KeyCode.T))
		{
			DamageHealth(2);
		}

		if (Input.GetKeyDown(KeyCode.J))
		{
			invul = !invul;
		}
	}

	public void DamageHealth(float _damage)
	{
		if(invul == true || isDead)
		{
			return;
		}

		health -= _damage;
        health = Mathf.Clamp(health, 0f, maxHealth);

		if(health <= 0)
		{
			isDead = true;
			animator.Play("Dead");
			if(callDead != null)
			{
				callDead();
			}
		
		}

		AudioManager.instance.Play("Hit");
		StartCoroutine("InvulSet");
		updateHealth(_damage,true);
	}

	public void RestoreHealth(float _heal)
	{
		health += _heal;
		health = Mathf.Clamp(health, 0f, maxHealth);
		updateHealth(_heal,false);
	}

	IEnumerator InvulSet()
	{
		invul = true;
		yield return new WaitForSeconds(0.7f);
		invul = false;
	}

	public void updatehealth()
	{
		switch (GameManager.instance.healthLevel)
		{
			case 1:
				maxHealth = 4;
				break;
			case 2:
				maxHealth = 5;
				break;
			case 3:
				maxHealth = 6;
				break;
			case 4:
				maxHealth = 7;
				break;
		}
		health = maxHealth;
	}
}
