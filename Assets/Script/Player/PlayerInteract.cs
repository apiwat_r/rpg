using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
	[SerializeField] private GameObject notifier;
	private void OnTriggerEnter2D(Collider2D collision)
	{
		RangeDetector r = collision.gameObject.transform.GetComponent<RangeDetector>();

		if (r != null)
		{
			if(collision.tag == "NPC")
			{
				notifier.SetActive(true);
			}

			r.SetInRange();
		}
	
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		if (collision.tag == "NPC")
		{
			collision.gameObject.transform.GetComponent<RangeDetector>().NotInRange();
			notifier.SetActive(false);
		}

			
	}
}
