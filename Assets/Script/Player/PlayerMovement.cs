using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    private float runSpeed = 40f;

    [SerializeField] private Animator playerAnimator;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private BoxCollider2D boxCollider2D;

    [SerializeField] private float _shootSpeed = 4f;
    [SerializeField] private float _normalSpeed = 6f;
    private CanvasManager canvasManager;
    public float shootSpeed
	{
        get
		{
            return _shootSpeed;
		}
       
	}
    public  float normalSpeed
	{
        get
		{
            return _normalSpeed;
		}

	}

    private NodeParser nodeParser;
    private PlayerStat playerStat;
    private PlayerSpecial playerSpecial;
    private Rigidbody2D rb;
    private Vector2 movement;
    private Vector2 dashingDirection;
    public bool dashing { get; private set; } = false;

	private void Awake()
	{
        playerSpecial = GetComponent<PlayerSpecial>();
        playerStat = GetComponent<PlayerStat>();
        canvasManager = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>();
        nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
        rb = GetComponent<Rigidbody2D>();
    }
	private void Start()
	{
	}

	void Update()
    {

		if (canvasManager.pause || playerStat.isDead)
		{
            return;
		}

		if (Input.GetKeyDown(KeyCode.LeftShift) && movement != new Vector2(0,0) && !dashing && !playerSpecial.isWhirling)
		{
            dashing = true;
            dashingDirection = movement.normalized * normalSpeed * 2f;
            AudioManager.instance.Play("Dodge");
            StartCoroutine("DashCountdown");
		}

		if (!nodeParser.inConvo && !dashing)
		{
            movement.x = Input.GetAxisRaw("Horizontal") * runSpeed;
            movement.y = Input.GetAxisRaw("Vertical") * runSpeed;
        }
		else if (!dashing)
		{
            movement.x = 0; 
            movement.y = 0;
        }

        RunningAnim();
    }

	private void FixedUpdate()
	{
        if (canvasManager.pause || playerStat.isDead)
        {
            return;
        }
        if (dashing)
		{
            rb.MovePosition(rb.position + dashingDirection * Time.deltaTime);
            boxCollider2D.enabled = false;
        }
		else
		{
            rb.MovePosition(rb.position + movement * Time.deltaTime);
            boxCollider2D.enabled = true;
        }
        
    }

	private void RunningAnim()
	{
        if (playerSpecial.isWhirling)
		{
            return;
		}
		if (dashing)
		{
            playerAnimator.Play("Dashing");
            return;
		}

        if (movement.x > 0)
        {
            playerAnimator.Play("Run");
            spriteRenderer.flipX = false;
        }
        else if (movement.x < 0)
        {
            playerAnimator.Play("Run");
            spriteRenderer.flipX = true;
        }
        else if (movement.y != 0)
        {
            playerAnimator.Play("Run");

        }
        else if (movement.x == 0 && movement.y == 0)
        {
            playerAnimator.Play("Idle");
        }
    }

    public Vector2 GetMovement()
	{
        return movement;
	}

    IEnumerator DashCountdown()
	{
        yield return new WaitForSeconds(0.3f);
        dashing = false;
    }

   public void changeSpeed(float _runSpeed)
	{
        runSpeed = _runSpeed;
	}

}
