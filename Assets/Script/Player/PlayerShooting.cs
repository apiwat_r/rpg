using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{

    Vector2 mousePos;
    private Transform bulletPoint;

    [SerializeField] private float Firerate;
    [SerializeField] private float bulletForce;
    [SerializeField] private GameObject firePoint;
    [SerializeField] private List<GameObject>  bulletPrefab = new List<GameObject>();

    private PlayerStat playerStat;
    private CanvasManager canvasManager;
    private GameObject crossHair;
    private Camera cam;
    private PlayerMovement playerMovement;
    private NodeParser nodeParser;
    private float nextTimeToFire = 0;
    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        playerStat = GetComponent<PlayerStat>();
        canvasManager = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>();
        cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        crossHair = GameObject.FindWithTag("Canvas").GetComponent<CanvasManager>().returnCrossHair();
        playerMovement = GetComponent<PlayerMovement>();
        nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
        Cursor.visible = false;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);

        CrossHairMove();
        GetPoint();

		if (canvasManager.pause || playerStat.isDead)
		{
            return;
		}

        if (Input.GetButton("Fire1") && !nodeParser.inConvo && !playerMovement.dashing)
        {
            playerMovement.changeSpeed(playerMovement.shootSpeed);

            if (Time.time >= nextTimeToFire)
			{
                nextTimeToFire = Time.time + 1f / Firerate;
                Shoot();
            }
        }
		else
		{
            playerMovement.changeSpeed(playerMovement.normalSpeed);
		}
    }

    private void CrossHairMove()
	{
        Vector2 cursorPos = Input.mousePosition;
        crossHair.transform.position = cursorPos;
	}

    private void GetPoint()
	{
        firePoint.transform.up = mousePos - (Vector2)firePoint.transform.position;
    }

    private void Shoot()
    {
        GameObject bullet = Instantiate(bulletPrefab[GameManager.instance.bulletLevel -1], firePoint.transform.position, firePoint.transform.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.transform.up * bulletForce, ForceMode2D.Impulse);

        AudioManager.instance.Play("Shoot",0.2f);
    }
}
