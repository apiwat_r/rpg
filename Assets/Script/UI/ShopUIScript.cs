using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopUIScript : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI moneyText;
    [Header("QuestTab")]
    [SerializeField] private GameObject QuestTab;
    [SerializeField] private Image QuestDetailImage;
    [SerializeField] private TextMeshProUGUI questDescription;

    [Header("UpgradeTab")]
    [SerializeField] private GameObject UpgradeTab;
    [SerializeField] private Image UpgradeDetailImage;
    [SerializeField] private TextMeshProUGUI UpgradeDescription;
    [SerializeField] private TextMeshProUGUI costText;
    [SerializeField] private Button buyButton;
    [SerializeField] private List<GameObject> upgradeSorter = new List<GameObject>();
    [SerializeField] private List<TextMeshProUGUI> levelText = new List<TextMeshProUGUI>();
    [SerializeField] private GameObject whilrImage;
    [SerializeField] private GameObject LaserImage;
    [SerializeField] private List<Sprite> spriteList = new List<Sprite>();


    [Header("Reference Object")]
    [SerializeField] private StatManager statManager;
    [SerializeField] private PlayerSpecial playerSpecial;

private RectTransform ShopUI;

    [Header("Tween")]
    [SerializeField] private float tweenTime;
    public LeanTweenType tweenTypeIn;
    public LeanTweenType tweenTypeOut;
    private int upgradeBuy = 0;

    private NodeParser nodeParser;
    public bool inShop { get; set; } = false;
    private int moneyRequire;
    private string levelName = "Level1";
    private PlayerStat playerStat;

    private CanvasManager canvasManager;
    private GameManager gameManager;

	private void Awake()
	{

        playerStat = GameObject.FindWithTag("Player").GetComponent<PlayerStat>();
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
        ShopToggle.toggleShop += ShopActivate;
        checkUpgrade();
	}

	private void OnDestroy()
	{
        ShopToggle.toggleShop -= ShopActivate;
    }

	private void Start()
	{
        ShopUI = GetComponent<RectTransform>();
        nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
        canvasManager = transform.parent.GetComponent<CanvasManager>();

    }

	private void Update()
	{
        if (Input.GetKeyDown(KeyCode.K))
        {
            GameManager.instance.addMoney(9999);
            updateMoney();
        }
    }

	public void updateMoney()
	{
        moneyText.text = "$" + GameManager.instance.money.ToString();
    }

    public void CallQuestTab()
    {
        QuestTab.SetActive(true);
        UpgradeTab.SetActive(false);
    }

    public void CallUpgradeTab()
    {
        QuestTab.SetActive(false);
        UpgradeTab.SetActive(true);
    }

    public void updateLevel()
	{
        levelName = EventSystem.current.currentSelectedGameObject.GetComponent<QuestInfoHolder>().GetQuest().questName;
        QuestDetailImage.sprite = EventSystem.current.currentSelectedGameObject.GetComponent<QuestInfoHolder>().GetQuest().questSprite;
        questDescription.text = EventSystem.current.currentSelectedGameObject.GetComponent<QuestInfoHolder>().GetQuest().questDescription;
    }

    public void updateShopButton()
	{
        Shop upgrade = EventSystem.current.currentSelectedGameObject.GetComponent<shopInfoHolder>().GetShop();
        UpgradeDetailImage.sprite =  upgrade.upgradeSprite;
        UpgradeDescription.text = upgrade.upgradeDescription;
        upgradeBuy = upgrade.upgradeID;
        moneyRequire = upgrade.cost[gameManager.upgradeIdentifier[upgradeBuy] -1];

        if (gameManager.upgradeIdentifier[upgradeBuy] - 1 >= 3)
        {
            costText.text = "MAX";
            buyButton.interactable = false;
        }
        else if(gameManager.money <= moneyRequire)
		{
            costText.text = "Buy:  $" + upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
            buyButton.interactable = false;
        }
        else
        {
            costText.text = "Buy: $" + upgrade.cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
            buyButton.interactable = true;
        }

    }

    public void changeLevel()
	{
        canvasManager.TransOut(levelName);
        AudioManager.instance.LerpMute("BaseTheme");
	}

    public void ShopActivate()
    {
        ShopUI.LeanMove(new Vector3(0, 0, 0), tweenTime).setEase(tweenTypeIn);
        updateMoney();
        nodeParser.ConvoOn();
        inShop = true;
        updateShopLevel();
        int ID = upgradeSorter[0].GetComponent<shopInfoHolder>().GetShop().upgradeID;
        moneyRequire = upgradeSorter[0].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[ID] -1] ;

        if (gameManager.upgradeIdentifier[upgradeBuy] - 1 >= 3)
        {
            costText.text = "MAX";
            buyButton.interactable = false;
        }
        else if (gameManager.money <= moneyRequire)
		{
            costText.text = "Buy:  $" + upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
            buyButton.interactable = false;
        }
        else
        {
            costText.text = "Buy:  $" + upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
            buyButton.interactable = true;
        }
    }

    public void ShopDisable()
    {
        StartCoroutine("_ShopDisable");
    }

    public IEnumerator _ShopDisable()
    {
        ShopUI.LeanMove(new Vector3(810, 0, 0), tweenTime).setEase(tweenTypeOut);
        yield return new WaitUntil(() => LeanTween.isTweening() == false);
        ShopUI.anchoredPosition = new Vector3(-810, 0, 0);
        nodeParser.ConvoOff();
        inShop = false;
        playerSpecial.checkUpgradeLevel();
    }

    public void upgrade()
	{
        gameManager.upgradeIncrease(upgradeBuy);
        AudioManager.instance.Play("Kaching");
        gameManager.minusMoney(moneyRequire);
        updateMoney();
        updateShopLevel();
        SaveManager.save(gameManager);

    }

    public void updateShopLevel()
    {
        moneyRequire = upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1];
        for (int i = 0; i < levelText.Count; i++)
        {
            switch (upgradeBuy)
            {
                case 0:
                    levelText[0].text = "Lv. " + gameManager.bulletLevel.ToString();
                    adjustMaxLevel(0,4);
                    break;
                case 1:
                    levelText[1].text = "Lv. " + gameManager.healthLevel.ToString();
                    playerStat.updatehealth();
                    statManager.instantHeart();
                    adjustMaxLevel(1,4);
                    break;
                case 2:
                    levelText[2].text = "Lv. " + gameManager.circleLevel.ToString();
                    adjustMaxLevel(2,4);
                    break;
                case 3:
                    levelText[3].text = "Lv. " + gameManager.railLevel.ToString();
                    adjustMaxLevel(3,4);
                    break;
            }

            if (gameManager.upgradeIdentifier[upgradeBuy] - 1 >= 3)
            {
                costText.text = "MAX";
                buyButton.interactable = false;
            }
            else if (gameManager.money <= moneyRequire)
            {
                costText.text = "Buy:  $" + upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
                buyButton.interactable = false;
            }
            else
            {
                costText.text = "Buy:  $" + upgradeSorter[upgradeBuy].GetComponent<shopInfoHolder>().GetShop().cost[gameManager.upgradeIdentifier[upgradeBuy] - 1].ToString();
                buyButton.interactable = true;
            }

        }
    }

    private void adjustMaxLevel(int index, int max)
	{
        if (levelText[index].text == "Lv. " + max.ToString())
        {
            levelText[index].text = "Max";
        }
    }

    private void checkUpgrade()
	{
        if (gameManager.allowWhirl)
        {
            whilrImage.GetComponent<Button>().interactable = true;
            whilrImage.GetComponent<Image>().sprite = spriteList[0];
        }
        else
        {
            whilrImage.GetComponent<Button>().interactable = false;
            whilrImage.GetComponent<Image>().sprite = spriteList[2];
        }

        if (gameManager.allowLaser)
        {
            LaserImage.GetComponent<Button>().interactable = true;
            LaserImage.GetComponent<Image>().sprite = spriteList[1];
        }
        else
        {
            LaserImage.GetComponent<Button>().interactable = false;
            LaserImage.GetComponent<Image>().sprite = spriteList[2];
        }
    }
}
