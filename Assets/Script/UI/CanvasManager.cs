using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class CanvasManager : MonoBehaviour
{
	//public static CanvasManager instance;

	public GameObject transitionScreen;

	[SerializeField] private GameObject pauseUI;
	[SerializeField] private RectTransform shopUI;
	[SerializeField] private GameObject crossHair;
	[SerializeField] private ShopUIScript shopUIScript;
	[SerializeField] private MissionCallout missionCallout;
	[SerializeField] private RectTransform deadUI;
	[SerializeField] private Transform checkPoint;
	[SerializeField] private GameObject statHolder;
	[SerializeField] private string Music;
	[SerializeField] private float volume;

	private GameObject whirlCooldown;
	private Image whirlOverlay;
	private GameObject laserCooldown;
	private Image laserOverlay;

	public bool pause { get; private set; } = false;
	private PlayerStat playerStat;
	private GameObject player;

	private GameManager gameManager;

	private void Awake()
	{

		gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();
		player = GameObject.FindWithTag("Player");
		
		if(player)
		{
			playerStat = player.GetComponent<PlayerStat>();
		}
		
		PlayerSpecial.CallWhirlCooldown += calWhirlCooldown;
		PlayerSpecial.CallLaserCooldown += calLaserCooldown;
		PlayerStat.callDead += CallDeadUI;

		if (statHolder)
		{
			whirlCooldown = statHolder.transform.GetChild(1).gameObject;
			whirlOverlay = whirlCooldown.transform.GetChild(1).GetComponent<Image>();
			laserCooldown = statHolder.transform.GetChild(2).gameObject;
			laserOverlay = laserCooldown.transform.GetChild(1).GetComponent<Image>();
		}


		if (gameManager.allowWhirl)
		{
			whirlCooldown.SetActive(true);
		}

		if (gameManager.allowLaser)
		{
			laserCooldown.SetActive(true);
		}
	}

	private void OnDestroy()
	{
		PlayerStat.callDead -= CallDeadUI;
		PlayerSpecial.CallWhirlCooldown -= calWhirlCooldown;
		PlayerSpecial.CallLaserCooldown -= calLaserCooldown;
	}

	private void Start()
	{
		AudioManager.instance.Play(Music, volume);
		if (gameManager.checkPointState == GameManager.CheckPointState.CheckPoint)
		{
			player.transform.position = checkPoint.position;
		}
		TransIn();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && !shopUIScript.inShop && !playerStat.isDead)
		{
			pause = !pause;
			pauseUI.SetActive(!pauseUI.activeSelf);

			if (pause)
			{
				Time.timeScale = 0;
			}
			else if (!pause)
			{
				Time.timeScale = 1;
			}
		}
	}

	public void TransOut(string _sceneName) 
	{
		pause = false;
		Time.timeScale = 1;
		if (shopUI)
		{
			shopUIScript.inShop = false;
		}

		LeanTween.alpha(transitionScreen.GetComponent<RectTransform>(), 1, 0.5f).setOnComplete(TransIn,_sceneName);
		AudioManager.instance.LerpMute("DungeonTheme");
		AudioManager.instance.LerpMute("BossTheme");
		AudioManager.instance.LerpMute("BaseTheme");
		AudioManager.instance.LerpMute("TitleTheme");
	}
	public void TransIn(object sceneName = null)
	{
		if (sceneName != null)
		{
			if (sceneName.ToString() != SceneManager.GetActiveScene().name)
			{
				gameManager.changeCheckPoint(GameManager.CheckPointState.spawnPoint);
			}
			SceneManager.LoadScene(sceneName.ToString());
		}
		transitionScreen.GetComponent<Image>().color = new Color(0, 0, 0, 1);
		LeanTween.alpha(transitionScreen.GetComponent<RectTransform>(), 0, 0.5f);
		if (shopUI)
		{
			shopUI.anchoredPosition = new Vector2(-810, 0);
		}
		
	}

	public GameObject returnCrossHair()
	{
		return crossHair;
	}

	public  MissionCallout returnMission()
	{
		return missionCallout;
	}

	public void CallDeadUI()
	{
		deadUI.LeanMove(new Vector3(0, 0, 0), 1f).setFrom(new Vector3(-676,0,0)).setEase(LeanTweenType.easeInOutCubic);
	}

	public void calWhirlCooldown(float _wCooldown)
	{
		whirlOverlay.fillAmount = _wCooldown;
	}
	public void calLaserCooldown(float _lCooldown)
	{
		laserOverlay.fillAmount = _lCooldown;
	}
}
