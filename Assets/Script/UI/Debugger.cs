using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class Debugger : MonoBehaviour
{
    [SerializeField] private GameObject mushroom;
    [SerializeField] private Transform spawnPoint;


    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Q))
		{
			AIDestinationSetter destinationSetter = mushroom.GetComponent<AIDestinationSetter>();
			destinationSetter.target = GameObject.FindWithTag("Player").transform;

			Instantiate(mushroom, spawnPoint.position, Quaternion.identity);
		}

		
		
	}
}
