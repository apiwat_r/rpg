using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragAndDrop : MonoBehaviour 
{

    private Vector3 _dragOffset;
	private Camera _cam;

	private void Awake()
	{
		_cam = Camera.main;
	}

	private void OnMouseDown()
	{
		_dragOffset = transform.position - GetMousePos();
	}

	private void OnMouseDrag()
	{
        transform.position = GetMousePos() + _dragOffset;
	}

    Vector3 GetMousePos()
	{
        var mousePos = _cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
        return mousePos;
	}
}
