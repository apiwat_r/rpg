using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
	public static GameManager instance;
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);


	}

	public Dictionary<int, int> upgradeIdentifier { get; private set; } = new Dictionary<int, int>();
	public int money  = 0;

	private int upgradeType;
	public int bulletLevel { get; private set; } = 1;
	public int healthLevel { get; private set; } = 1;
	public int dropLevel { get; private set; } = 1;
	public int railLevel { get; private set; } = 1;
	public int circleLevel { get; private set; } = 1;

	public bool allowWhirl = false;
	public bool allowLaser = false;
	public bool tutorial = false;
	public int stageReached = 1;

	public enum CheckPointState {spawnPoint,CheckPoint }
	public CheckPointState checkPointState { get; private set; } = CheckPointState.spawnPoint;

	private void Start()
	{

		upgradeIdentifier.Add(0, bulletLevel);
		upgradeIdentifier.Add(1, healthLevel);
		upgradeIdentifier.Add(2, dropLevel);
		upgradeIdentifier.Add(3, railLevel);
		upgradeIdentifier.Add(4, circleLevel);
		SaveManager.load();

	}

	//Function//

	public void addMoney(int _moneyAdd)
	{
		money += _moneyAdd;
		money = Mathf.Clamp(money, 0, 9999);
	}
	public void minusMoney(int _moneyMinus)
	{
		money -= _moneyMinus;
	}
	public float GetMoney()
	{
		return money;
	}

	public void ChangeStage(int _stageReach)
	{
		if(stageReached < _stageReach)
		{
			stageReached = _stageReach;
		}
	
	}

	public void tutorialCheck(bool _check)
	{
		tutorial = _check;
	}

	public void changeCheckPoint(CheckPointState _state)
	{
		checkPointState = _state;
	}
	
	public void upgradeIncrease(int d)
	{
		
		switch (d) {
			case 0:
				if(bulletLevel < 4)
				{
					bulletLevel += 1;
					upgradeIdentifier[d] = bulletLevel;
				}

				break;
			case 1:
				if (healthLevel < 4)
				{
					healthLevel += 1;
					upgradeIdentifier[d] = healthLevel;
				}
					break;
			case 2:
				if (circleLevel < 4)
				{
					circleLevel += 1;
					upgradeIdentifier[d] = circleLevel;
				}
					break;
			case 3:
				if (railLevel < 4)
				{
					railLevel += 1;
					upgradeIdentifier[d] = railLevel;
				}
					break;
			case 4:
				if (dropLevel < 4)
				{
					dropLevel += 1;
					upgradeIdentifier[d] = dropLevel;
				}
					break;
		}
			
	}

	[ContextMenu("Save")]
	public void save()
	{
		SaveManager.save(this);
	}

	[ContextMenu("Load")]
	public void Load()
	{
		 SaveData data = SaveManager.load();
		money = data.money;
		bulletLevel = data.bulletLevel;
		healthLevel = data.healthLevel;
		railLevel = data.railLevel;
		circleLevel = data.circleLevel;
		stageReached = data.stageReach;
		tutorial = data.tutorial;
		allowWhirl = data.allowWhirl;
		allowLaser = data.allowLaser;
	}
}

[System.Serializable]
public class SaveData
{
	public int money = 0;
	public int bulletLevel = 1;
	public int healthLevel = 1;
	public int dropLevel = 1;
	public int railLevel = 1;
	public int circleLevel = 1;
	public int stageReach = 1;
	public bool tutorial = false;
	public bool allowWhirl = false;
	public bool allowLaser = false;
	public SaveData(GameManager gameManager)
	{
		money = gameManager.money;
		bulletLevel = gameManager.bulletLevel;
		healthLevel = gameManager.healthLevel;
		dropLevel = gameManager.dropLevel;
		railLevel = gameManager.railLevel;
		circleLevel = gameManager.circleLevel;
		stageReach = gameManager.stageReached;
		tutorial = gameManager.tutorial;
		allowWhirl = gameManager.allowWhirl;
		allowLaser = gameManager.allowLaser;
	}
}
