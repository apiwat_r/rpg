using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeDetector : MonoBehaviour
{
	public bool inRange { get; private set; } = false;

	public  void SetInRange()
	{
		inRange = true;
	}

	public void NotInRange()
	{
		inRange = false;
	}

}
