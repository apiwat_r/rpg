using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroller : MonoBehaviour
{
    [SerializeField] RawImage scroller;
    [SerializeField] float x, y;
    // Start is called before the first frame update

    // Update is called once per frame
    void Update()
    {
        scroller.uvRect = new Rect(scroller.uvRect.position + new Vector2(x, y) * Time.deltaTime, scroller.uvRect.size);
    }
}
