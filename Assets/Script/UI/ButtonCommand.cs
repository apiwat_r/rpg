using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonCommand : MonoBehaviour
{
	[SerializeField] private GameObject creditPage;
    public void ChangeScene(string sceneName)
	{
		SceneManager.LoadScene(sceneName);
	}

	public void enableCredit()
	{
		creditPage.SetActive(!creditPage.activeSelf);
	}

	public void Exit()
	{
		Application.Quit();
	}
}
