using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class StatManager : MonoBehaviour
{
	[SerializeField] private GameObject heartSorter;
	[SerializeField] private int healthIndex;
	[SerializeField] private GameObject heart;
	private int heartAmount = 0;
	// Start is called before the first frame update
	private void OnEnable()
	{
		PlayerStat.updateHealth += ChangeUIHealth;
	}

	private void OnDisable()
	{

		PlayerStat.updateHealth -= ChangeUIHealth;
	}

	private void Start()
	{
		instantHeart();
		healthIndex = heartSorter.transform.childCount-1;
	}

	public void ChangeUIHealth(float health, bool damage)
    {
		if (damage)
		{
			for (var i = 0; i < health ; i++)
			{
				if (healthIndex <= -1)
				{
					return;
				}
				heartSorter.transform.GetChild(healthIndex).GetComponent<Animator>().Play("HeartAnim");
				healthIndex -= 1;
			}
		}
		else
		{
			for (var i = 0; i < health; i++)
			{
				if (healthIndex >= heartSorter.transform.childCount -1)
				{
					return;
				}
					healthIndex += 1;
				heartSorter.transform.GetChild(healthIndex).GetComponent<Animator>().Play("HeartAnimReverse");
			}
		}

    }

	public void instantHeart()
	{
		switch (GameManager.instance.healthLevel)
		{
			
			case 1:
				heartAmount = 4;
				break;
			case 2:
				heartAmount = 5;
				break;
			case 3:
				heartAmount = 6;
				break;
			case 4:
				heartAmount = 7;
				break;
		}

		for (int i = heartSorter.transform.childCount; i < heartAmount; i++)
		{
			Instantiate(heart, heartSorter.transform);
		}
	}
}
