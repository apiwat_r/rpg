using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestInfoHolder : MonoBehaviour
{
    [SerializeField] private Quest quest;
    [SerializeField] private int questNumber;
	private Button button;

	private void Start()
	{
		button = GetComponent<Button>();
		if (GameManager.instance.stageReached >= questNumber)
		{
			button.interactable = true;
		}

	}

	private void Update()
	{
		
	}

	public Quest GetQuest()
    {
        return quest;
    }

}
