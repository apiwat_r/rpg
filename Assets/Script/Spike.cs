using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
	private Animator animator;
	private BoxCollider2D boxCollider2D;

	private void Awake()
	{
		EnemySpawner.toggleSpike += DisableSpike;
	}

	private void Start()
	{
		animator = GetComponent<Animator>();
		boxCollider2D = GetComponent<BoxCollider2D>();
	}

	private void OnDestroy()
	{
		EnemySpawner.toggleSpike -= DisableSpike;
	}

	public void DisableSpike(bool _check)
	{
		if (_check)
		{
			animator.Play("Spike");
			boxCollider2D.enabled = true;
		}
		else
		{
			animator.Play("SpikeDisable");
			boxCollider2D.enabled = false;
		}
	}

}
