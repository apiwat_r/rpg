using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShaderController : MonoBehaviour
{

    [SerializeField] private Color _mainColor;
    [SerializeField] private Color _secondaryColor;

    private SpriteRenderer sprite;
    private DialougeBank dialougeBank;


    // Start is called before the first frame update
    void Start()
    {
        dialougeBank = GetComponent<DialougeBank>();
        // We do this GetComponent() only once
        sprite = GetComponent<SpriteRenderer>();
        if (sprite.material != null) // We dodge the null ref exception
            sprite.material = Instantiate(sprite.material);
        sprite.material.SetTexture("_MainText", sprite.sprite.texture);
    }

}
