using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonAttack : EnemyAttack
{
    // Start is called before the first frame update
    void Start()
    {
        Initialize();   
    }

    // Update is called once per frame
    void Update()
    {
        attacking();

        print(player);
    }

	public override void attacking()
	{
        switch (enemyState)
        {
            case AIstate.walking:

                animator.Play("Walk");
                AIpath.canMove = true;

                if (AIpath.reachedEndOfPath)
                {
                    enemyState = AIstate.attacking;
                }

                break;

            case AIstate.attacking:

                AIpath.canMove = false;

                animator.Play("Attack");

                if (!isAttacking && AIpath.remainingDistance > AIpath.endReachedDistance)
                {
                    enemyState = AIstate.walking;
                }
                break;
        }
    }


}
