using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
	public static Action<bool> toggleSpike;
	private bool current = false;
	public int enemyCount = 0;
	private int waveIndex = 0;
	private bool finish = false;
	[SerializeField] private bool spawn = false;

	[SerializeField] private WaveSetting[] waveSettings;
	[SerializeField] private Boundary[] boundaries;

	[SerializeField]private bool[] BoundaryDisable;

	[SerializeField] private GameObject spawnEffect;
	// Start is called before the first frame update

	private void Awake()
	{
		EnemyStat.enemyDeath += UpdateDeathCount;
	}

	private void OnDestroy()
	{
		EnemyStat.enemyDeath -= UpdateDeathCount;
	}

	private void Start()
	{
		if(waveSettings.Length != 0)
		{
			enemyCount = GetEnemycount();
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		current = true;
		if (spawn)
		{
			if(toggleSpike != null)
			{
				toggleSpike(true);
			}
	

			if (waveSettings.Length != 0)
			{
				StartCoroutine(spawnEnemyLocation());
			}
		}
	}

	private void OnTriggerExit2D(Collider2D collision)
	{
		current = false;
	}

	IEnumerator spawnEnemyLocation()
	{
		yield return new WaitForSeconds(0.5f);
		
		for (var i = 0; i < waveSettings[waveIndex].enemyType.Count ; i++)
		{
			for (var x = 0; x < waveSettings[waveIndex].enemyType[i].amount; x++)
			{
				int choosenBoundary = (UnityEngine.Random.Range(1, boundaries.Length));

				while (BoundaryDisable[choosenBoundary] == false)
				{
					choosenBoundary = (UnityEngine.Random.Range(1, boundaries.Length));
				}

				Vector2 spawnLocation = calculateBoundary(choosenBoundary - 1);

				yield return new WaitForSeconds(0.1f);
				StartCoroutine(spawnEnemy(spawnLocation, i));
			}
		}
	}

	IEnumerator spawnEnemy(Vector2 spawnLocation, int index)
	{
		AudioManager.instance.Play("Puff");
		GameObject effect = Instantiate(spawnEffect, spawnLocation, Quaternion.identity);
		yield return new WaitForSeconds(0.5f);
		Destroy(effect);
		Instantiate(waveSettings[waveIndex].enemyType[index].enemy, spawnLocation, Quaternion.identity);
	}

	public int GetEnemycount()
	{
		int totalEnemy = 0;

		for (var i = 0; i < waveSettings[waveIndex].enemyType.Count; i++)
		{
			totalEnemy += waveSettings[waveIndex].enemyType[i].amount;
		}
		return totalEnemy;
	}

	public Vector2 calculateBoundary(int _Number)
	{
		Vector2 spawnLocation = new Vector2(UnityEngine.Random.Range(boundaries[_Number].transform1.position.x, boundaries[_Number].transform2.position.x)  , UnityEngine.Random.Range(boundaries[_Number].transform1.position.y, boundaries[_Number].transform2.position.y));

		return spawnLocation;
	}

	private void UpdateDeathCount()
	{
		if (current)
		{
			enemyCount -= 1;
		}

		if(enemyCount <= 0 && current)
		{
			toggleSpike(false);
			spawn = false;
		}
	}

	public void useSpike(bool _bool)
	{
		toggleSpike(_bool);
	}
}

[System.Serializable]
public class Boundary
{
	public Transform transform1;
	public Transform transform2;
}
