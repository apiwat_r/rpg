using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDrop : MonoBehaviour
{

    [SerializeField] private int dropMin, dropMax;
    [SerializeField] private GameObject deadDrop;
	// Start is called before the first frame update


	public void DropItem()
	{
        int amount = Random.Range(dropMin, dropMax -1);
        for (var i = 0; i < amount; i++)
        {
           GameObject D = Instantiate(deadDrop, transform.position, Quaternion.identity);
        }
    }
}
