using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyGraphic : MonoBehaviour
{
	public AIPath aiPath;
	public enum GraphicType { facingPlayer, walkDirection }
	[SerializeField] private GraphicType graphicType;
	GameObject player;

	private void Start()
	{
		player = GameObject.FindWithTag("Player");
	}
	// Update is called once per frame
	void Update()
    {
		if(graphicType == GraphicType.walkDirection)
		{
			if (aiPath.desiredVelocity.x >= 0.01f)
			{
				transform.localScale = new Vector3(1f, 1f, 1f);

			}
			else if (aiPath.desiredVelocity.x <= -0.01f)
			{
				transform.localScale = new Vector3(-1f, 1f, 1f);
			}
		}

		if (graphicType == GraphicType.facingPlayer)
		{
			Vector2 direction = (player.transform.position - transform.position).normalized;

			if (direction.x >=  0.1)
			{
				transform.localScale = new Vector3(1f, 1f, 1f);

			}
			else if (direction.x <= -0.1f)
			{
				transform.localScale = new Vector3(-1f, 1f, 1f);
			}
		}

	}

	public void changeGraphic(GraphicType _graphicType)
	{
		graphicType = _graphicType;
	}

}
