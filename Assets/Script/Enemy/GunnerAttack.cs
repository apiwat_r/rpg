using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunnerAttack : EnemyAttack
{
    [SerializeField] private float bulletForce;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private GameObject firePoint;
    [SerializeField] private LayerMask wallCheck;
    
    // Start is called before the first frame update
    void Start()
    {

        Random.InitState(Random.Range(1,90000));
        Initialize();

        AIpath.destination = CalWalk();
        StartCoroutine(walkCountdown());
    }

    // Update is called once per frame
    void Update()
    {
        GetPoint();
        attacking();

        print(AIpath.destination);

    }

    public override void attacking()
    {
        switch (enemyState)
        {
            case AIstate.walking:

                animator.Play("Fly");
                AIpath.canMove = true;
    
                break;

            case AIstate.attacking:

                AIpath.canMove = false;

                animator.Play("Attack");

                break;
        }
    }

    public void spawnBullet()
	{
        GameObject bullet = Instantiate(bulletPrefab, firePoint.transform.position, firePoint.transform.rotation);
        Rigidbody2D rb = bullet.GetComponent<Rigidbody2D>();
        rb.AddForce(firePoint.transform.up * bulletForce, ForceMode2D.Impulse);
    }

    private void GetPoint()
    {
        firePoint.transform.up = (Vector2)player.transform.position - (Vector2)firePoint.transform.position;
    }

    public void ChangeToWalk()
    {
        AIpath.destination = CalWalk();
        enemyState = AIstate.walking;
        StartCoroutine(walkCountdown());

    }

    public Vector2 CalWalk()
    {
        Vector2 direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f) ) * 100;

       // RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 5, wallCheck);

        return direction;
    }

    IEnumerator walkCountdown()
	{
        yield return new WaitForSeconds(Random.Range(1f,2f));
        enemyState = AIstate.attacking;
    }
}
