using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeAttack : EnemyAttack
{
    private bool changeDirection = false;
    private bool coRunning = false;
    // Start is called before the first frame update
    void Start()
    {
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        attacking();
    }

	public override void attacking()
	{
        animator.Play("Move");

        if (!coRunning)
        {
            StartCoroutine(DirectionTimer());
        }


        switch (enemyState)
        {
            case AIstate.walking:

                if (AIpath.reachedEndOfPath || changeDirection)
                {
                    AIpath.destination = CalWalk();
                    changeDirection = false;
                }

                break;

            case AIstate.attacking:

                AIpath.destination = player.transform.position;
                break;
        }
    }

    public Vector2 CalWalk()
    {
        Vector2 direction = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * 10;

        // RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, 5, wallCheck);

        return direction;
    }

    IEnumerator DirectionTimer()
    {
        coRunning = true;
        yield return new WaitForSeconds(1f);

        int i = Random.Range(1, 3);


		switch (i) 
        {
            case 1:
                enemyState = AIstate.walking;
                changeDirection = true;
                break;
            case 2:
                enemyState = AIstate.attacking;
                break;
        }
        coRunning = false;
 

    }
}
