using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BanditAttack : EnemyAttack
{
   
    public enum AttackState { dynamite, circling, charge }

    [Header("BossStat")]
    [SerializeField] private float moveTimer;
    [SerializeField] private AttackState attackState;
    [SerializeField] private float bulletAmount;
    [SerializeField] private float bulletForce;
    [SerializeField] private float endAngle, StartAngle;
    [SerializeField] private float shotCooldown = 0;
    [SerializeField] private float radius;
    //[SerializeField] private int bombAmount = 0;
    [SerializeField] private float chargeSpeed = 0;
    private float _shotCooldown;

    [Header("Reference Object")]
    [SerializeField] private GameObject bulletPoint;
    [SerializeField] private GameObject bulletChild;
    [SerializeField] private GameObject bullet;
    //[SerializeField] private GameObject bombWaypoint;
    //[SerializeField] private GameObject bomb;
    [SerializeField] private List<GameObject> waypoint = new List<GameObject>();

    [SerializeField]private EnemyGraphic enemyGraphic;
    private Rigidbody2D rb;
    private Vector2 pos;
    private Vector2 target;
    private int currentWaypoint;
    private float _moveTimer;
    private bool initiate = false;
    private bool getTarget = false;
    private bool getTargetCircling = false;
    private bool firstRun = false;


    // Start is called before the first frame update
    void Start()
    {
        _moveTimer = moveTimer;
        rb = GetComponent<Rigidbody2D>();
        _shotCooldown = shotCooldown;
        Initialize();

        IntBullet();
    }

    // Update is called once per frame
    void Update()
    {
		if (!active)
		{
            return;
		}

		if (!firstRun)
		{
            animator.Play("Run");
            firstRun = true;
		}
        moveTimer -= Time.deltaTime;
        switch (attackState)
        {
            case AttackState.circling:

                enemyGraphic.changeGraphic(EnemyGraphic.GraphicType.facingPlayer);
                if (moveTimer <= 0)
				{
                    attackState = AttackState.charge;
                    moveTimer = _moveTimer;
				}

                bulletPoint.transform.up = (Vector2)player.transform.position - (Vector2)bulletPoint.transform.position;
                AIpath.canMove = true;

                if (initiate == false)
                {
                    AIpath.maxSpeed = 8;

                    if(getTargetCircling  == false)
					{
                       int random = Random.Range(0, 4);
                        AIDestinationSetter.target = waypoint[random].transform;
                        currentWaypoint = random;
                        getTargetCircling = true;
                    }

					if (AIpath.reachedDestination)
					{
                        initiate = true;
                        currentWaypoint = (currentWaypoint + 1) % waypoint.Count;
                        AIDestinationSetter.target = waypoint[currentWaypoint].transform;
                    }
                  
                }

                if (initiate == true)
                {
                    shotCooldown -= Time.deltaTime;

                    if (shotCooldown <= 0)
                    {
                        animator.Play("Attack");
                        AudioManager.instance.Play("BanditAttack");
                        ShootBullet();
                        shotCooldown = _shotCooldown;
                    }

                    if (AIpath.reachedDestination)
                    {
                        currentWaypoint = (currentWaypoint + 1) % waypoint.Count;
                        AIDestinationSetter.target = waypoint[currentWaypoint].transform;
                    }
                }

                break;
        }
    }

	private void FixedUpdate()
	{
        if (!active)
        {
            return;
        }
        if (attackState == AttackState.charge)
		{
            enemyGraphic.changeGraphic(EnemyGraphic.GraphicType.walkDirection);
            getTargetCircling = false;
            initiate = false;
            if (moveTimer <= 0)
            {
                attackState = AttackState.circling;
                moveTimer = _moveTimer;
            }
            Charge();
        }
	}

	private void IntBullet()
    {
        float angleStep = (endAngle - StartAngle) / bulletAmount;

        float offsetAngle = StartAngle;

        pos = new Vector2(bulletPoint.transform.position.x, bulletPoint.transform.position.y);

        for (var i = 0; i < bulletAmount + 1; i++)
        {
            Instantiate(bulletChild, pos + (Vector2FromAngle((offsetAngle + (angleStep * i))) * radius), Quaternion.identity, bulletPoint.transform);
        }
    }

    private void ShootBullet()
    {
        for (var i = 0; i < bulletPoint.transform.childCount; i++)
        {
            Vector2 bulDir = bulletPoint.transform.GetChild(i).transform.position - bulletPoint.transform.position;

            GameObject bulletInstance = Instantiate(bullet, bulletPoint.transform.position, bulletPoint.transform.rotation);
            Rigidbody2D rb = bulletInstance.GetComponent<Rigidbody2D>();
            rb.AddForce(bulDir * bulletForce, ForceMode2D.Impulse);
        }
    }

    //IEnumerator throwBomb()
    //{
    //    throwingBomb = true;

    //    for (var i = 0; i < bombAmount; i++)
    //    {
    //        GameObject bombInstant = Instantiate(bomb, transform.position, Quaternion.identity);
    //        bombInstant.LeanMove(new Vector3(transform.position.x + (Random.Range(-11f, 11f)), transform.position.y + (Random.Range(-6f, 6f))), 1f).setEase(LeanTweenType.easeInCubic);
    //    }

    //    yield return new WaitForSeconds(2f);

    //    throwingBomb = false;
    //}
    private void Charge()
	{
        AIpath.canMove = false;

        if(getTarget == false)
		{
            target = ((player.transform.position - transform.position)).normalized;
            getTarget = true;
        }

        rb.velocity = target * chargeSpeed * Time.deltaTime;
    }

    public Vector2 Vector2FromAngle(float a)
    {
        a *= Mathf.Deg2Rad;
        return new Vector2(Mathf.Cos(a), Mathf.Sin(a));
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
       
		if (attackState == AttackState.charge && collision.transform.tag == "Wall")
		{
            pos = new Vector2(bulletPoint.transform.position.x, bulletPoint.transform.position.y);

            for (var i = 0; i < 8; i++)
			{
                GameObject bulletInstance = Instantiate(bullet, bulletPoint.transform.position, Quaternion.identity);
                Rigidbody2D rb = bulletInstance.GetComponent<Rigidbody2D>();
                rb.AddForce(((Vector2)bulletPoint.transform.position + (Vector2FromAngle(45f * i) - (Vector2)transform.position)).normalized * bulletForce,ForceMode2D.Impulse);
            }
            getTarget = false;
		}
	}
}
