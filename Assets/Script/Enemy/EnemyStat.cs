using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using Pathfinding;

public class EnemyStat : MonoBehaviour
{
	public static Action bossDeath;
	public static Action enemyDeath;
	public static Action<float,MissionCallout.CallType> updateBossHealth;
	[SerializeField] private bool boss;
	[SerializeField] private bool bossBandit;
	[SerializeField] private bool bossWizard;
	[SerializeField] private bool bossGolem;
	[SerializeField] private string Bossname;
    [SerializeField] private float health = 1;
    [SerializeField] private GameObject deadEffect;
	
	private float maxHealth;
	private EnemyDrop enemyDrop;

	// Start is called before the first frame update
	void Start()
    {
		maxHealth = health;
		enemyDrop = GetComponent<EnemyDrop>();
    }

	public void takeDamage(float damage)
	{
		AudioManager.instance.Play("EnemyHit");
        health -= damage;

		if (boss)
		{
			float remainingHealth = health / maxHealth;
			if (bossBandit)
			{
				updateBossHealth(remainingHealth, MissionCallout.CallType.whirl);
			}
			else if (bossWizard)
			{
				updateBossHealth(remainingHealth, MissionCallout.CallType.laser);
			}
			else if (bossGolem)
			{
				updateBossHealth(remainingHealth, MissionCallout.CallType.end);
			}
			else
			{
				updateBossHealth(remainingHealth, MissionCallout.CallType.complete);
			}

		}

		if(boss && health <= 0)
		{
			Instantiate(deadEffect, transform.position, Quaternion.identity);
			enemyDrop.DropItem();
			Destroy(gameObject);

		}

        else if(health  <= 0)
		{
			if(enemyDeath != null)
			{
				enemyDeath();
			}

			Instantiate(deadEffect, transform.position, Quaternion.identity);
			enemyDrop.DropItem();
			Destroy(gameObject);
		}
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.transform.parent != null && collision.transform.parent.tag == "Player")
		{
			PlayerStat p = collision.transform.parent.GetComponentInParent<PlayerStat>();
			p.DamageHealth(1);
		}
	}

	public void kill()
	{
		if (!boss) 
		{
			Destroy(gameObject);
		}
	}
}
