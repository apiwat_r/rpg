using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Pathfinding;

public class EnemyAttack : MonoBehaviour
{
    protected GameObject player;
    protected bool isAttacking = false;
    public enum AIstate {walking,attacking};
    public enum AItype { Distance, follow, teleport, specific };
    [SerializeField] protected AItype enemyType;
    [SerializeField] protected AIstate enemyState;
    protected AIDestinationSetter AIDestinationSetter;
    protected bool active = false;
    protected AIPath AIpath;
    protected Animator animator;
    public static Action<string> callBossHealth;
    [SerializeField]protected string bossName;

    public virtual void attacking()
	{
  
    }

    public void Initialize()
	{
        player = GameObject.FindWithTag("Player");
        AIDestinationSetter = GetComponent<AIDestinationSetter>();
        AIpath = GetComponent<AIPath>();
        animator = GetComponent<Animator>();

        if (enemyType == AItype.follow)
        {
            AIDestinationSetter.target = player.transform;
        }
    }

    public virtual void setAttack()
    {
        isAttacking = true;
    }

    public virtual void turnOffAttack()
    {
        isAttacking = false;
    }

    public virtual void setActive()
	{
        active = true;
        callBossHealth(bossName);
	}

    public virtual void setDeactive()
    {
        active = false;

    }
}
