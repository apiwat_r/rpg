using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	[SerializeField] private GameObject hitEffect;
	[SerializeField] private float damage;
	public enum BulletType { enemy,player}
	[SerializeField] private BulletType bulletType;
	[SerializeField] private Renderer _renderer;
	[SerializeField] private bool AllowDestroy = true;

	private void Start()
	{
		StartCoroutine(destoryBullet());
	}

	private void Update()
	{
		if (!_renderer.isVisible && bulletType == BulletType.player)
		{
			Destroy(gameObject);
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		GameObject hit = Instantiate(hitEffect, transform.position, Quaternion.identity);

		switch (bulletType)
		{
			case BulletType.player:

				if (collision.gameObject.tag == "Enemy")
				{
					collision.gameObject.GetComponent<EnemyStat>().takeDamage(damage);
				}

				Destroy(hit, 0.5f);
				Destroy(gameObject);
				break;

			case BulletType.enemy:

				print("F");
				if (collision.transform.parent.tag == "Player")
				{
					collision.gameObject.GetComponent<PlayerStat>().DamageHealth(damage);
				}
				break;
		}
	}


	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(bulletType == BulletType.enemy)
		{
			if (collision.transform.parent != null && collision.transform.parent.tag == "Player")
			{
				collision.transform.parent.GetComponent<PlayerStat>().DamageHealth(damage);
			}
		}

		if(collision.tag  == "SuperBullet")
		{
			GameObject hit = Instantiate(hitEffect, transform.position, Quaternion.identity);
			Destroy(hit, 0.5f);
			Destroy(gameObject);
		}
	}

	IEnumerator destoryBullet()
	{
		yield return new WaitForSeconds(4);
		if (AllowDestroy) {
			Destroy(gameObject);
		}
	
	}

	public BulletType returnType()
	{
		return bulletType;
	}
}
