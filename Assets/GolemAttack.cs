using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GolemAttack : EnemyAttack
{
    public enum AttackState { rotating, wipe ,transition}

    [Header("BossStat")]
    [SerializeField] private float moveTimer;
    [SerializeField] private AttackState attackState;
    private AttackState previousAttack;
    [SerializeField] private float laserCooldown = 0;
    [SerializeField] private float shotCooldown = 0;
    [SerializeField] private int bombAmount = 0;
    private float _shotCooldown;

    [Header("Reference Object")]
    [SerializeField] private GameObject bombWaypoint;
    [SerializeField] private GameObject bomb;
    [SerializeField] private GameObject shield;
    [SerializeField] private GameObject shield2;
    [SerializeField] private GameObject Laser;
    [SerializeField] private GameObject Laser2;
    [SerializeField] private GameObject shieldPivot;
    [SerializeField] private GameObject LaserHorizontal;
    [SerializeField] private GameObject LaserVertical;
    [SerializeField] private List<GameObject> waypoint = new List<GameObject>();

    [SerializeField] private EnemyGraphic enemyGraphic;
    private Rigidbody2D rb;
    private Vector2 pos;
    private Vector2 target;
    private bool getTarget;
    private float _moveTimer;
    private bool initiate = false;
    private bool throwingBomb;


    // Start is called before the first frame update
    void Start()
    {
        _moveTimer = moveTimer;
        rb = GetComponent<Rigidbody2D>();
        _shotCooldown = shotCooldown;
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {

        if (!active)
        {
            return;
        }

        shield.transform.RotateAround(shieldPivot.transform.position, new Vector3(0, 0, 1), 100 * Time.deltaTime);
        shield2.transform.RotateAround(shieldPivot.transform.position, new Vector3(0, 0, 1), 100 * Time.deltaTime);
        moveTimer -= Time.deltaTime;

        if (moveTimer <= 0)
        {
            LeanTween.scale(Laser, new Vector3(0.15f, 0.00f, 0.1f), 0.1f).setEase(LeanTweenType.linear);
            LeanTween.scale(Laser2, new Vector3(0.15f, 0.00f, 0.1f), 0.1f).setEase(LeanTweenType.linear);
            initiate = false;
            previousAttack = attackState;
            attackState = AttackState.transition;
            StartCoroutine(randomAttackState());
            moveTimer = _moveTimer;
        }
        switch (attackState)
        {
            case AttackState.rotating:

                AIpath.maxSpeed = 8;

                if (initiate == false)
                {
                    if (getTarget == false)
                    {
                        AIDestinationSetter.target = bombWaypoint.transform;
                        getTarget = true;
                    }

                    if (AIpath.reachedDestination && getTarget == true)
                    {
                        Laser.SetActive(true);
                        Laser2.SetActive(true);
                        LeanTween.scale(Laser, new Vector3(0.15f, 0.11f, 0.0f), 0.5f).setFrom(new Vector3(0.15f, 0f, 0.1f)).setEase(LeanTweenType.easeInOutCirc).setOnComplete(enableCollision);
                         LeanTween.scale(Laser2, new Vector3(0.15f, 0.11f, 0.0f), 0.5f).setFrom(new Vector3(0.15f, 0f, 0.1f)).setEase(LeanTweenType.easeInOutCirc)
;                       initiate = true;
                        int random = Random.Range(0, 7);
                        AIDestinationSetter.target = waypoint[random].transform;
                    }
                }
                if (initiate == true)
                {
                    AIpath.maxSpeed = 5;

					if (AIpath.reachedDestination)
					{
                        int random = Random.Range(0, 7);
                        AIDestinationSetter.target = waypoint[random].transform;
                    }

                }
        
                break;

            case AttackState.wipe:
                if (initiate == false)
                {
                    if (getTarget == false)
                    {
                        AIDestinationSetter.target = bombWaypoint.transform;
                        getTarget = true;
                    }

                    if (AIpath.reachedDestination && getTarget == true)
                    {
                        initiate = true;
                        
                        
                    }
                }
                if (initiate == true)
                {
                    shotCooldown -= Time.deltaTime;
                    if (shotCooldown <= 0)
                    {
                        Instantiate(bomb, player.transform.position, Quaternion.identity);
                        shotCooldown = _shotCooldown;
                    }

                    laserCooldown -= Time.deltaTime;
					if (laserCooldown <= 0)
					{
                        int i = Random.Range(0, 2);
                        int t = Random.Range(0, 2);
                        if (i == 0)
						{
                            GameObject laserHor = Instantiate(LaserHorizontal, new Vector3(100, 100, -2), Quaternion.identity);
                            if (t == 0)
							{
                                LeanTween.move(laserHor, new Vector3(-1.2f, 6, -2), 3f).setFrom(new Vector3(-1.2f, -10, -2)).setOnComplete(destroyBeam, laserHor);
                            }
							else
							{
                                LeanTween.move(laserHor, new Vector3(-1.2f, -10, -2), 3f).setFrom(new Vector3(-1.2f, 6, -2)).setOnComplete(destroyBeam, laserHor);
                            }
                           
						}
						else
						{
                            GameObject laserVer = Instantiate(LaserVertical, new Vector3(100, 100, -2), Quaternion.Euler(0,0,90));
                            if (t == 0)
                            {
                                LeanTween.move(laserVer, new Vector3(14, -3, -2), 3f).setFrom(new Vector3(-13, -3, -2)).setOnComplete(destroyBeam, laserVer);
                            }
                            else
                            {
                                LeanTween.move(laserVer, new Vector3(-13, -3, -2), 3f).setFrom(new Vector3(14, -3, -2)).setOnComplete(destroyBeam,laserVer);
                            }
                        }

                        laserCooldown = Random.Range(1.5f, 2.5f);
					}
                }
                break;
        }
    }

    public void  destroyBeam(object Beam)
	{
        GameObject _Beam = Beam as GameObject;
        Destroy(_Beam);
	}
    public  void enableCollision()
	{
        BoxCollider2D a = Laser.GetComponent<BoxCollider2D>();
        BoxCollider2D b = Laser2.GetComponent<BoxCollider2D>();
        a.enabled = !a.enabled;
        b.enabled = !b.enabled;
	}

    IEnumerator randomAttackState()
    {
        AttackState _attackState = (AttackState)Random.Range(0, 2);

        while (_attackState == attackState || _attackState == previousAttack)
        {
            _attackState = (AttackState)Random.Range(0, 2);
        }

        yield return new WaitForSeconds(0.3f);
        attackState = _attackState;
    }

}
