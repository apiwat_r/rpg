using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoofEffect : MonoBehaviour
{
    private Animator animator;
    [SerializeField] private SpriteRenderer wizardSprite;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Disappear()
	{
        animator.Play("SmokePoof");
	}

    public void DisableWizard()
	{
        wizardSprite.enabled = false;
	}

	public void Destroy()
	{
        Destroy(transform.parent.gameObject);
	}
}
