using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shopInfoHolder : MonoBehaviour
{
	[SerializeField] private Shop shop;
    public Shop GetShop()
    {
        return shop;
    }
}
