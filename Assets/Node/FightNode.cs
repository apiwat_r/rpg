﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class FightNode : BaseNode {

	[Input] public int entry;
	[Output(dynamicPortList = true)] public int[] exit;
	[SerializeField] private EnemyAttack enemyAttack;


}