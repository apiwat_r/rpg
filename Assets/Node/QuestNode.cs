﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class QuestNode : BaseNode {

	[Input] public int entry;
	[Output(dynamicPortList = true)] public int[] exit;
	public Quest quest;
	public bool completed;

	public override Quest GetQuest()
	{
		return quest;
	}

	public override bool GetBool()
	{
		return completed;
	}

}