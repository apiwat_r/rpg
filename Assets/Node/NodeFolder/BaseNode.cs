﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class BaseNode : Node {

	public virtual string GetString()
	{
		return null;
	}

	public virtual Sprite GetSprite()
	{
		return null;
	}

	public virtual string GetList(int listIndex)
	{
		return null;
	}

	public virtual Quest GetQuest()
	{
		return null;
	}

	public virtual bool GetBool()
	{
		return false;
	}

	public virtual GameEvent GetEvent()
	{
		return null;
	}
}