﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class EventNode : BaseNode {

	[Input] public int entry;
	[Output(dynamicPortList = true)] public string[] exit;

	public GameEvent gameEvent;

	public override GameEvent GetEvent()
	{
		return gameEvent;
	}
}