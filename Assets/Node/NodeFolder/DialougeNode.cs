﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialougeNode : BaseNode {

	
	[Input] public int entry;
	[Output(dynamicPortList = true)] public int[] exit;
	public string speakerName;
	public string dialougeLine;
	public Sprite sprite;
	

	public override string GetString()
	{
		return "DialougeNode/" + speakerName + "/" + dialougeLine;
	}

	public override Sprite GetSprite()
	{
		return sprite;
	}

}