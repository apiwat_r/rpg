﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class ChoiceNode : BaseNode {

	[Input] public int entry;
	[Output(dynamicPortList = true)] public string[] exit;
	public List<string> choiceString = new List<string>();

	public override object GetValue(NodePort port)
	{
		return port;
	}

	public override string GetList(int listIndex)
	{
		return choiceString[listIndex];
	}

}