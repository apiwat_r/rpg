using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialougeChoice : MonoBehaviour
{
	public NodeParser nodeParser;
	public TextMeshProUGUI choiceText;
	public int ChoiceNumber = -1;

	public void ChoiceChoose()
	{
		nodeParser.choiceChoose(ChoiceNumber);
	}
}
