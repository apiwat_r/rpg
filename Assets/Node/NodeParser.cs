using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using XNode;

public class NodeParser : MonoBehaviour
{
    [SerializeField] private DialougeGraph graph;
    [SerializeField] private EnemyAttack enemyAttack;
    Coroutine _parser;
    IEnumerator _textCoroutine;
    [SerializeField]private GameObject dialougeBox;
    private TextMeshProUGUI speaker;
    private TextMeshProUGUI dialouge;
    [SerializeField]  private Image speakerImage;

    private DialougeboxManager dialougeboxManager;
    [SerializeField] private GameObject choiceBox;
    private GameObject QuestNotifier;
    private GameManager gameManager;

    public bool inConvo { get; private set; } = false;
    private bool endOfDialouge = false;
    private int choiceMade =  -1;
    private int choiceNumber = -1;
    private string dialougeText = "";

    [SerializeField]private float textSpeed = 1f;
    [SerializeField] private GameObject dialougeChoice;
    

	private void Start()
	{
        gameManager = GameObject.FindWithTag("GameManager").GetComponent<GameManager>();

        textSpeed = 1 / textSpeed;

        speaker = dialougeBox.transform.GetChild(1).transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        dialouge = dialougeBox.transform.GetChild(0).transform.GetChild(1).GetComponent<TextMeshProUGUI>();

        dialougeboxManager = dialougeBox.GetComponent<DialougeboxManager>();
	}

	private void Update()
	{
        SkipText(dialougeText);
	}

	// Start is called before the first frame update
	public IEnumerator InitiateConver()
	{
        endOfDialouge = false;
        foreach (BaseNode b in graph.nodes)
        {
            if (b.GetString() == "Start")
            {
                graph.current = b;
                break;
            }
        }

        yield return new WaitUntil(() => Input.GetKeyUp(KeyCode.E));
        _parser = StartCoroutine(ParseNode());
    }

    public void AutoConver()
	{
        endOfDialouge = false;
        foreach (BaseNode b in graph.nodes)
        {
            if (b.GetString() == "Start")
            {
                graph.current = b;
                break;
            }
        }
        _parser = StartCoroutine(ParseNode());
    }

    public  void callEvent()
	{
        graph.current.GetEvent().Invoke();
	}

	IEnumerator ParseNode()
	{
        inConvo = true;
        BaseNode b = graph.current;
        string data = b.GetString();
        string[] dataParts = data.Split('/');
        dialougeboxManager.SetTargetVisible();

        if (dataParts[0] == "Start")
        {
            NextNode("exit");
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.E));
            yield return new WaitUntil(() => Input.GetKeyUp(KeyCode.E));
        }
        else if (dataParts[0] == "DialougeNode")
		{
           
            speaker.text = dataParts[1];
            dialougeText = dataParts[2];
            speakerImage.sprite = b.GetSprite();

            dialouge.text = "";

            _textCoroutine = TypeText(dataParts[2].ToCharArray(), dataParts[2]);

            StartCoroutine(_textCoroutine);

            yield return new WaitUntil(() => dialouge.text == dataParts[2]);
            yield return new WaitUntil(() => Input.GetKeyDown(KeyCode.E));
            yield return new WaitUntil(() => Input.GetKeyUp(KeyCode.E));

            NextNode("exit 0");
		}
	}

    IEnumerator TypeText(char[] d, string b)
	{
        foreach (char letter in d)
        {
            dialouge.text += letter;
            
            if(dialouge.text == b)
			{
                yield break;
			}

            yield return new WaitForSeconds(textSpeed);
        }
    }
	IEnumerator ParseChoice()
	{
        choiceNumber = -2;

		foreach (NodePort p in graph.current.Outputs)       
		{
			choiceNumber += 1;  

            if(choiceNumber >= 0)
			{
               
                dialougeChoice.GetComponent<DialougeChoice>().choiceText.text = graph.current.GetList(choiceNumber);

                dialougeChoice.GetComponent<DialougeChoice>().nodeParser = transform.GetComponent<NodeParser>();

				dialougeChoice.GetComponent<DialougeChoice>().ChoiceNumber = choiceNumber;

				Instantiate(dialougeChoice, choiceBox.transform);
			}
		}

		yield return new WaitUntil(() => choiceMade != -1);

        for(var i = 0; i < choiceBox.transform.childCount ; i++)
		{
            Destroy(choiceBox.transform.GetChild(i).gameObject);
		}

		NextNode("exit " + choiceMade.ToString());
        choiceMade = -1;
    }

    public void NextNode(string fieldName)
	{
        if(_parser != null)
		{
            StopCoroutine(_parser);
            _parser = null;
		}       
        foreach (NodePort p in graph.current.Ports)
		{
            if(p.fieldName == fieldName)
			{
                if(p.Connection != null)
				{
                    graph.current = p.Connection.node as BaseNode;
                    break;
                }
				else
				{
                    dialougeboxManager.SetTargetInvisible();
                    inConvo = false;
                    endOfDialouge = true;
				}
			}
		}

		switch (graph.current.name)
		{
			case "Choice":
				_parser = StartCoroutine(ParseChoice());
				break;
			case "Dialouge":

                if (!endOfDialouge)
                {
                    _parser = StartCoroutine(ParseNode());
                }
                break;
            case "Fight":
                dialougeboxManager.SetTargetInvisible();
                inConvo = false;
                endOfDialouge = true;
                AudioManager.instance.Play("BossTheme",0.2f);
                enemyAttack.setActive();
                break;
            case "Event":

                dialougeboxManager.SetTargetInvisible();
                inConvo = false;
                endOfDialouge = true;
                callEvent();
                break;

		}

    }

    public void choiceChoose(int _choicechoose)
	{
        choiceMade = _choicechoose;
	}

    public void GetGraph(DialougeGraph dialougeGraph)
	{
        graph = dialougeGraph;
	}

    public void GetEnemy(EnemyAttack _enemyAttack)
    {
        enemyAttack = _enemyAttack;
    }

    public void SkipText(string a)
	{
        if(_textCoroutine == null)
		{
            return;
		}

		if (Input.GetKeyUp(KeyCode.E) && inConvo)
        {
            StopCoroutine(_textCoroutine);

            dialouge.text = a;
        }
	}

    public void ConvoOn()
	{
        inConvo = true;
	}

    public void ConvoOff()
    {
        inConvo = false;
    }

}
