using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialougeBank : MonoBehaviour
{
    [SerializeField] private DialougeGraph graph;
	private NodeParser nodeParser;
	private SpriteRenderer sprite;
	private RangeDetector rangeDetector;

	private void Start()
	{
		rangeDetector = GetComponent<RangeDetector>();
		sprite = GetComponent<SpriteRenderer>();
		nodeParser = GameObject.FindWithTag("DialougeManager").GetComponent<NodeParser>();
	}

	public DialougeGraph GetGraph()
	{
		return graph;
	}

	private void Update()
	{
		if (rangeDetector.inRange)
		{
			sprite.material.SetFloat("_Thickness", 0.005f);
		}
		else
		{
			sprite.material.SetFloat("_Thickness", 0f);
		}

		if (rangeDetector.inRange && !nodeParser.inConvo && Input.GetKeyDown(KeyCode.E))
		{
			nodeParser.GetGraph(graph);
			StartCoroutine(nodeParser.InitiateConver());
		}
	}


	private void OnMouseExit()
	{
		sprite.material.SetFloat("_Thickness", 0f);
	}
}
